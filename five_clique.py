"""
# Copyright (C) 2022 - Benjamin Paassen

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import csv
from tqdm import tqdm
import json

# Here, now, begins the daunting task of finding five-cliques in the graph we
# prepared via 'generate_graph.py'.

print('--- loading graph ---')

# load the graph first
words = []
with open('word_graph_reduced.csv', newline='', encoding='utf-8') as f:
	reader = csv.reader(f, delimiter = '\t')
	for row in tqdm(reader):
		word = row[0]
		neighbors = set(json.loads(row[1]))
		words.append((row[0], neighbors))

print('--- start clique finding (THIS WILL TAKE LONG!) ---')

# start clique finding
Cliques = []
for i in tqdm(range(len(words))):
	Ni = words[i][1]
	for j in Ni:
		# the remaining candidates are only the words in the intersection
		# of the neighborhood sets of i and j
		Nij = Ni & words[j][1]
		for k in Nij:
			# intersect with neighbors of k
			Nijk = Nij & words[k][1]
			for l in Nijk:
				# intersect with neighbors of l
				Nijkl = Nijk & words[l][1]
				# all remaining neighbors form a 5-clique with i, j, k, and l
				for r in Nijkl:
					Cliques.append([i, j, k, l, r])

print('completed! Found %d cliques' % len(Cliques))

print('--- write to output ---')
with open('cliques.csv', 'w', newline='', encoding='utf-8') as f:
	writer = csv.writer(f, delimiter = '\t')
	for cliq in Cliques:
		# get word representation of cliques and write to output
		cliq_words = [words[i][0] for i in cliq]
		writer.writerow(cliq_words)
